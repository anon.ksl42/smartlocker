import HTTP from "./ApiService.js";

export default {
  getLockerAllById(id) {
    return HTTP().get(`lockers?lockerId=${id}`);
  },
  checkStatusLocker(id) {
    return HTTP()
      .get(`lockers/checkStatus?lkRoomId=${id}`)
      .catch(function(error) {
        const { status, data } = error.response;
        if (status == 400) {
          return status;
        }
      });
  },
  openLocker(data) {
    return HTTP()
      .get(
        `lockers/embedded/command/2?password=${data.passCode}&lkRoomId=${data.lkRoomId}`
      )
      .catch(function(error) {
        const { status, data } = error.response;
        if (status == 422) {
          return data.message;
        } else if (status == 400) {
          return data.message;
        } else if (status == 401) {
          return status;
        } else if (status == 500) {
          return status;
        }
      });
  },
};
