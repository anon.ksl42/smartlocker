import axios from "axios";
import { BASEURL } from "../config/index";

export default () => {
  return axios.create({ baseURL: BASEURL });
};
