import LockerService from "../services/LockerService";
export const LockerStore = {
  namespaced: true,
  state: {
    items: [],
    openStatus: false,
    statusBooking: false,
  },
  mutations: {
    SET_ITEMS(state, payload) {
      state.items = payload;
    },
    SET_OPENSTATUS(state, payload) {
      state.openStatus = payload;
    },
    SET_STATUSBOOKING(state, payload) {
      state.statusBooking = payload;
    },
  },
  getters: {
    items(state) {
      return state.items;
    },
    openStatus(state) {
      return state.openStatus;
    },
    statusBooking(state) {
      return state.statusBooking;
    },
  },
  actions: {
    async getLocker({ commit }, data) {
      let res = await LockerService.getLockerAllById(data);
      const { content } = res.data;

      for (let i in content) {
        content[i].color = "#F7CB05";
        if (content[i].status === "A") {
          content[i].subtitleThai = "ว่าง";
          content[i].subtitleEng = "available";
          content[i].disable = false;
        }
        if (content[i].status === "B") {
          content[i].subtitleThai = "จองแล้ว";
          content[i].subtitleEng = "booking";
          content[i].disable = false;
        }
        if (content[i].status === "F") {
          content[i].subtitleThai = "ซ่อมบำรุง";
          content[i].subtitleEng = "maintenance";
          content[i].disable = true;
        }
      }
      //   console.log("object :>> ", content);
      commit("SET_ITEMS", res.data.content);
    },
    async openLocker({ commit }, data) {
      let res = await LockerService.openLocker(data);
      if (res == "Password incorrect") {
        commit("SET_OPENSTATUS", false);
      } else if (res == 500) {
        commit("SET_OPENSTATUS", res);
      } else if (res == 401) {
        commit("SET_OPENSTATUS", res);
      } else {
        commit("SET_OPENSTATUS", res.data);
      }
    },
    async checkStatus({ commit }, data) {
      let res = await LockerService.checkStatusLocker(data);
      if (res == 400) {
        commit("SET_STATUSBOOKING", false);
      } else {
        commit("SET_STATUSBOOKING", res.data.content);
      }
    },
  },
};
