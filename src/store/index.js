import Vue from "vue";
import Vuex from "vuex";
import { LockerStore } from "./lockerStore";
Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    locker: LockerStore,
  },
});
